package com.toru.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.GridLayout
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.toru.myapplication.Adapter.ResultAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val listResult = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        rlResult.layoutManager = LinearLayoutManager(this)
        rlResult.layoutManager = GridLayoutManager(this,3)

    }

    fun Click(sender: View) {
//        val builder = AlertDialog.Builder(this)
//        builder.setTitle("1234567")
//        builder.setMessage(etTest.text)
//        builder.setPositiveButton("click") { _, _ -> null }
//        builder.show()
//        Toast.makeText(this,etTest.text,Toast.LENGTH_SHORT).show
        val result = etFirst.text.toString().toInt() + etSecond.text.toString().toInt()
        txtResult.text = result.toString()

//        val intent = Intent(this,SecondActivity::class.java)
//        intent.putExtra("result",result.toString())
//        startActivity(intent)
    }

    fun clickCalculate(sender: View){
        var result = 0.0
        when(sender.tag){
            "+" -> result = etFirst.text.toString().toDouble() + etSecond.text.toString().toDouble()
            "-" -> result = etFirst.text.toString().toDouble() - etSecond.text.toString().toDouble()
            "*" -> result = etFirst.text.toString().toDouble() * etSecond.text.toString().toDouble()
            "/" -> result = etFirst.text.toString().toDouble() / etSecond.text.toString().toDouble()
        }
//        txtResult.text = "${etFirst.text} ${sender.tag} ${etSecond.text} = $result"

        listResult.add("${etFirst.text} ${sender.tag} ${etSecond.text} = $result")

        rlResult.adapter = ResultAdapter(listResult)

//        for( resultXxX in listResult){
//            val builder = AlertDialog.Builder(this)
//            builder.setMessage(resultXxX)
//            builder.setPositiveButton("ok") { _, _ -> null }
//            builder.show()
//        }
    }
}
